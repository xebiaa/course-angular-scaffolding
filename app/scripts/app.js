'use strict';

/**
 * @ngdoc overview
 * @name angularjsScaffoldingAndToolingApp
 * @description
 * # angularjsScaffoldingAndToolingApp
 *
 * Main module of the application.
 */
angular
  .module('angularjsScaffoldingAndToolingApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'restangular'
  ])
  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/user');

    $stateProvider
      .state('login', {
        url: '/login',
        controller: 'LoginCtrl',
        templateUrl: 'views/login.html'
      })
      .state('main', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .state('main.user', {
        url: '/user',
        controller: 'UserCtrl',
        templateUrl: 'views/user.html'
      })
      .state('main.profiles', {
        url: '/profiles',
        controller: 'ProfilesCtrl',
        templateUrl: 'views/profiles.html'
      })
      .state('main.profile', {
        url: '/profiles/{profileId:[\\w]+}',
        templateUrl: 'views/profile.html',
        controller: 'ProfileCtrl'
      });
  })
  .config(function (RestangularProvider) {
    RestangularProvider.setDefaultHeaders({
      'X-Parse-Application-Id': "XyZFlYuOjUqUIyCMuDqeAD0r64bneJnF7pHETFfU",
      'X-Parse-REST-API-Key': "mN2bFmRclOdmemhpYrxubrI7fFVEJViyTZXUE1vP"
    });
  })
  .run(function (Restangular) {
    Restangular.setBaseUrl('https://api.parse.com/1');
  });

angular
  .module('angularjsScaffoldingAndToolingApp')
  .factory('Users', function(Restangular) {
    return Restangular.service('users');
  })
  .factory('Login', function(Restangular) {
    return Restangular.service('login');
  })
  .factory('Post', function(Restangular) {
    return Restangular.service('classes/Post');
  });
