'use strict';

angular.module('angularjsScaffoldingAndToolingApp')
  .controller('LoginCtrl', function ($cookieStore, $scope, $state, Users, Restangular) {

    function user() {
      return {
        username: $scope.username,
        password: $scope.password
      };
    }

    function loggedIn(result) {
      result = _.extend(result, user())
      $cookieStore.put('sessionToken', result.sessionToken);
      $cookieStore.put('username', result.username);
      $cookieStore.put('userId', result.objectId)

      $state.go('main.user');
    }

    function loginFailed() {
      $scope.error = 'Login failed';
    }

    $scope.signup = function () {
      Users.post(user()).then(loggedIn).catch(loginFailed);
    }

    $scope.login = function() {
      Restangular
        .one('login')
        .get(user()).then(loggedIn).catch(loginFailed);
    }
  });
