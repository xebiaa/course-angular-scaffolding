'use strict';

angular.module('angularjsScaffoldingAndToolingApp')
  .controller('MainCtrl', function ($scope, $state, $cookieStore, Users) {
    $scope.username = $cookieStore.get('username');
    if (!$scope.username) {
      $state.go('login');
    }

    function goToLogin() {
      $state.go('login');
    }

    $scope.logout = function () {
      $cookieStore.remove('username');
      $cookieStore.remove('sessionToken');
      $cookieStore.remove('userId');
      goToLogin();
    }

    Users.one('me').get({}, {'X-Parse-Session-Token': $cookieStore.get('sessionToken')}).catch(goToLogin);
  });
