'use strict';

angular.module('angularjsScaffoldingAndToolingApp')
  .controller('ProfileCtrl', function ($scope, $state, $stateParams, Post, Users) {

    Users.one($stateParams.profileId).get({}).then(function (result) {
      $scope.user = result;
    });

    var poster = {
      __type: "Pointer",
      className: "_User",
      objectId: $stateParams.profileId
    };

    Post.one('').get({
        where: angular.toJson({poster: poster})
      }).then(function (result) {
      $scope.posts = result.results;
    });

  });
