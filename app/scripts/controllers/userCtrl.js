'use strict';

angular.module('angularjsScaffoldingAndToolingApp')
  .controller('UserCtrl', function ($scope, $state, $cookieStore, $filter, Post) {
    var acl = {};
    acl[$cookieStore.get('userId')] = {read: true, write: true};
    acl['*'] = {read: true};

    var poster = {
      __type: "Pointer",
      className: "_User",
      objectId: $cookieStore.get('userId')
    };

    $scope.send = function () {
      var post = {
        text: $scope.input,
        poster: poster,
        ACL: acl
      };
      Post.post(post)
        .then(function (result) {
          _.extend(post, result);
          $scope.posts.push(post);
          $scope.input = '';
        });
    }

    Post.one('').get({
        where: angular.toJson({poster: poster})
      }).then(function (result) {
      $scope.posts = result.results;
    });
  });
