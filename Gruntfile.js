// Generated on 2014-06-26 using generator-angular 0.9.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // load all grunt config
  require('load-grunt-config')(grunt, {
    data: {
      // Project settings
      yeoman: {
        // Configurable paths for the application
        app: require('./bower.json').appPath || 'app',
        dist: 'dist'
      }
    }
  });
  grunt.loadTasks('grunt/tasks');

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);
};
