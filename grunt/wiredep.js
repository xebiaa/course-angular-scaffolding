// Automatically inject Bower components into the app
module.exports = {
  options: {
    cwd: '<%= yeoman.app %>'
  },
  app: {
    src: ['<%= yeoman.app %>/index.html'],
    ignorePath:  /..\//
  }
};
